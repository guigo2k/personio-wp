
worker_processes auto;

error_log /proc/self/fd/2 warn;
pid       /tmp/nginx.pid;

events {
    worker_connections 1024;
}

http {
    proxy_temp_path       /tmp/nginx/proxy;
    client_body_temp_path /tmp/nginx/client;
    fastcgi_temp_path     /tmp/nginx/fastcgi;

    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    log_format main '$remote_addr - $remote_user [$time_local] "$request" '
                    '$status $body_bytes_sent "$http_referer" '
                    '"$http_user_agent" "$http_x_forwarded_for"';

    access_log /proc/self/fd/2 main;

    sendfile on;
    tcp_nopush on;
    keepalive_timeout 65;
    gzip on;

    server {

        server_name _;
        listen 8080 default_server;
        root /personio/web/;

        add_header X-Frame-Options "SAMEORIGIN";
        add_header X-XSS-Protection "1; mode=block";
        add_header X-Content-Type-Options "nosniff";

        index index.html index.htm index.php;

        charset utf-8;

        location / {
            try_files $uri $uri/ /index.php?$query_string;
        }

        location = /favicon.ico { access_log off; log_not_found off; }
        location = /robots.txt  { access_log off; log_not_found off; }

        error_page 404 /index.php;

        location ~ \.php$ {
            fastcgi_pass unix:/tmp/php-fpm.sock;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
            include /etc/nginx/fastcgi_params;
        }

        location ~ /\.(?!well-known).* {
            deny all;
        }
    }
}
