#!/usr/bin/dumb-init /bin/sh

set -e

# load .env file
if [[ -f .env ]]; then
  . .env
else
  DATABASE_URL="${DATABASE_URL:?'Undefined environment variable'}"
  WP_ENV="${WP_ENV:?'Undefined environment variable'}"
  WP_TITLE="${WP_TITLE:?'Undefined environment variable'}"
  WP_HOME="${WP_HOME:?'Undefined environment variable'}"
  WP_SITEURL="${WP_SITEURL:?'Undefined environment variable'}"
  WP_USER="${WP_USER:?'Undefined environment variable'}"
  WP_EMAIL="${WP_EMAIL:?'Undefined environment variable'}"
fi

# wait mysql server
DB_HOST=$(dirname `echo $DATABASE_URL | cut -d@ -f2`)
while ! $(mysqladmin ping -h "$DB_HOST" --silent); do
  let COUNT++ && [[ $COUNT == 20 ]] && exit 1
  sleep 5
done

# install wordpress
if ! $(wp core is-installed); then
  wp core install \
    --url="$WP_HOME" \
    --title="$WP_TITLE" \
    --admin_user="$WP_USER" \
    --admin_email="$WP_EMAIL"
fi

/usr/sbin/nginx -c "$ROOT/config/nginx.conf"
/usr/sbin/php-fpm7 -y "$ROOT/config/php-fpm.conf"
