FROM alpine:3.8
LABEL maintainer="guigo@guigo2k.com"

ENV PORT="8080" \
    USER="nginx" \
    ROOT="/personio"

ENV PATH="$ROOT/vendor/bin:$PATH"

RUN set -x && \
    apk add --update \
      composer \
      curl \
      dumb-init \
      jq \
      mysql-client \
      nginx \
      php7-cli \
      php7-ctype \
      php7-curl \
      php7-dev \
      php7-dom \
      php7-exif \
      php7-fpm \
      php7-ftp \
      php7-gd \
      php7-gettext \
      php7-gmagick \
      php7-iconv \
      php7-imagick \
      php7-json \
      php7-mbstring \
      php7-mcrypt \
      php7-memcached \
      php7-mysqli \
      php7-opcache \
      php7-openssl \
      php7-pdo \
      php7-pdo_mysql \
      php7-pdo_pgsql \
      php7-pdo_sqlite \
      php7-pear \
      php7-phar \
      php7-posix \
      php7-redis \
      php7-simplexml \
      php7-soap \
      php7-sockets \
      php7-ssh2 \
      php7-tokenizer \
      php7-xml \
      php7-xmlreader \
      php7-xmlwriter \
      php7-zip \
      php7-zlib \
      ssmtp \
      zip

WORKDIR $ROOT

# cache parallel composer installs
ADD composer.json composer.lock $ROOT/
RUN composer global require hirak/prestissimo && \
    composer install --prefer-dist

# code base
ADD . $ROOT

# dumb-init entrypoint script
ADD docker-entrypoint.sh /usr/bin/run

# allow wordpress to send emails
ADD config/ssmtp.conf /etc/ssmtp/ssmtp.conf

# permissions for unprivileged nginx
RUN mkdir -p /tmp/nginx/{proxy,client,fastcgi} && \
    chown -R $USER:$USER $ROOT /tmp /usr/bin/run

USER $USER
EXPOSE $PORT

ENTRYPOINT ["/usr/bin/run"]
