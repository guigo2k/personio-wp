terraform {
  backend "s3" {
    bucket = "rz2-infra-dev-us-east-1"
    key    = "terraform/infra-dev-us-east-1/eks-cluster.tfstate"
    region = "us-east-1"
  }
}
