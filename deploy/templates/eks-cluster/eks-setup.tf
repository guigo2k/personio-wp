# # provider "kubernetes" {}
#
# locals {
#   cluster_name    = "${module.eks_cluster.eks_cluster_id}"
#   worker_role_arn = "${module.eks_workers.worker_role_arn}"
# }
#
# # # Create or update kubeconfig file (.kube/config)
# # resource "null_resource" "kube_config" {
# #   count = "${var.enabled == "true" ? 1 : 0}"
# #   depends_on = ["module.eks_workers"]
# #
# #   provisioner "local-exec" {
# #     interpreter = ["sh", "-c"]
# #
# #     command = <<EOF
# # aws eks --region ${var.aws_region} update-kubeconfig --name ${local.cluster_name} && sleep 10
# # EOF
# #   }
# # }
#
# data "external" "aws_iam_authenticator" {
#   program = ["bash", "${path.module}/eks-auth.sh"]
#
#   query {
#     cluster_name = "${local.cluster_name}"
#   }
# }
#
# provider "kubernetes" {
#   host                   = "${module.eks_cluster.eks_cluster_endpoint}"
#   cluster_ca_certificate = "${base64decode(module.eks_cluster.eks_cluster_certificate_authority_data)}"
#   token                  = "${data.external.aws_iam_authenticator.result.token}"
#   load_config_file       = false
# }
#
# # Enable worker nodes to join the cluster
# resource "kubernetes_config_map" "aws_auth" {
#   count      = "${var.enabled == "true" ? 1 : 0}"
#   # depends_on = ["null_resource.kube_config"]
#
#   metadata {
#     name      = "aws-auth"
#     namespace = "kube-system"
#   }
#
#   data {
#     mapRoles = <<EOF
# - rolearn: ${local.worker_role_arn}
#   username: system:node:{{EC2PrivateDNSName}}
#   groups:
#     - system:bootstrappers
#     - system:nodes
# EOF
#   }
# }
#
# # Wait workers to join the cluster
# resource "null_resource" "wait_workers" {
#   count      = "${var.enabled == "true" ? 1 : 0}"
#   depends_on = ["kubernetes_config_map.aws_auth"]
#
#   provisioner "local-exec" {
#     interpreter = ["sh", "-c"]
#
#     command = <<EOF
# until $(kubectl get nodes | grep -w -q "Ready"); do
#   sleep 3
# done
# EOF
#   }
# }
#
# # Create EKS admin service account
# resource "kubernetes_service_account" "eks_admin" {
#   count      = "${var.enabled == "true" ? 1 : 0}"
#   depends_on = ["null_resource.wait_workers"]
#
#   metadata {
#     name      = "eks-admin"
#     namespace = "kube-system"
#   }
# }
#
# # Create EKS admin cluster role binding
# resource "kubernetes_cluster_role_binding" "eks_admin" {
#   count      = "${var.enabled == "true" ? 1 : 0}"
#   depends_on = ["null_resource.wait_workers"]
#
#   metadata {
#     name = "eks-admin"
#   }
#
#   role_ref {
#     api_group = "rbac.authorization.k8s.io"
#     kind      = "ClusterRole"
#     name      = "cluster-admin"
#   }
#
#   subject {
#     kind      = "ServiceAccount"
#     name      = "eks-admin"
#     namespace = "kube-system"
#   }
# }
#
# # Create Helm/Tiller service account
# resource "kubernetes_service_account" "helm_tiller" {
#   count      = "${var.enabled == "true" ? 1 : 0}"
#   depends_on = ["null_resource.wait_workers"]
#
#   metadata {
#     name      = "tiller"
#     namespace = "kube-system"
#   }
# }
#
# # Create Helm/Tiller cluster role binding
# resource "kubernetes_cluster_role_binding" "helm_tiller" {
#   count      = "${var.enabled == "true" ? 1 : 0}"
#   depends_on = ["null_resource.wait_workers"]
#
#   metadata {
#     name = "tiller"
#   }
#
#   role_ref {
#     api_group = "rbac.authorization.k8s.io"
#     kind      = "ClusterRole"
#     name      = "cluster-admin"
#   }
#
#   subject {
#     kind      = "ServiceAccount"
#     name      = "tiller"
#     namespace = "kube-system"
#   }
# }
#
# # Deploy Tiller (helm init)
# resource "null_resource" "helm_tiller" {
#   count = "${var.enabled == "true" ? 1 : 0}"
#
#   depends_on = [
#     "kubernetes_service_account.helm_tiller",
#     "kubernetes_cluster_role_binding.helm_tiller",
#   ]
#
#   provisioner "local-exec" {
#     interpreter = ["sh", "-c"]
#
#     command = <<EOF
# helm init --service-account tiller --history-max 200
# EOF
#   }
# }
#
# # Deploy Kubernetes Dashboard
# resource "null_resource" "kube_dashboard" {
#   count      = "${var.enabled == "true" ? 1 : 0}"
#   depends_on = ["null_resource.wait_workers"]
#
#   provisioner "local-exec" {
#     interpreter = ["sh", "-c"]
#
#     command = <<EOF
# kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/${var.kube_dashboard_version}/src/deploy/recommended/kubernetes-dashboard.yaml
# kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/heapster.yaml
# kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/influxdb.yaml
# kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/rbac/heapster-rbac.yaml
# EOF
#   }
# }
#
# # Deploy Cert-Manager
# resource "null_resource" "cert_manager" {
#   count      = "${var.enabled == "true" ? 1 : 0}"
#   depends_on = ["null_resource.kube_dashboard"]
#
#   provisioner "local-exec" {
#     interpreter = ["sh", "-c"]
#
#     command = <<EOF
# kubectl create namespace cert-manager
# kubectl label namespace cert-manager certmanager.k8s.io/disable-validation=true
# kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/${var.cert_manager_version}/deploy/manifests/cert-manager.yaml
# kubectl apply -f letsencrypt-prod.yaml
# kubectl apply -f letsencrypt-staging.yaml
# EOF
#   }
# }
