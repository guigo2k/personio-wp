# This `label` is needed to prevent `count can't be computed` errors
module "label" {
  source     = "../../modules/null-label"
  namespace  = "${var.aws_name}"
  stage      = "${var.aws_env}"
  name       = "${var.aws_resource}"
  delimiter  = "${var.delimiter}"
  attributes = "${var.attributes}"
  tags       = "${var.tags}"
  enabled    = "${var.enabled}"
}

# This `label` is needed to prevent `count can't be computed` errors
module "cluster_label" {
  source     = "../../modules/null-label"
  namespace  = "${var.aws_name}"
  stage      = "${var.aws_env}"
  name       = "${var.aws_resource}"
  delimiter  = "${var.delimiter}"
  attributes = ["${compact(concat(var.attributes, list("cluster")))}"]
  tags       = "${var.tags}"
  enabled    = "${var.enabled}"
}

locals {
  # The usage of the specific kubernetes.io/cluster/* resource tags below are required
  # for EKS and Kubernetes to discover and manage networking resources
  # https://www.terraform.io/docs/providers/aws/guides/eks-getting-started.html#base-vpc-networking
  tags = "${merge(var.tags, map("kubernetes.io/cluster/${module.label.id}", "shared"))}"
}

module "eks_cluster" {
  source                  = "../../modules/eks-cluster"
  namespace               = "${var.aws_name}"
  stage                   = "${var.aws_env}"
  name                    = "${var.aws_resource}"
  attributes              = "${var.attributes}"
  tags                    = "${var.tags}"
  vpc_id                  = "${data.aws_vpc.network.id}"
  subnet_ids              = ["${data.aws_subnet_ids.public.ids}"]
  allowed_security_groups = ["${data.aws_security_groups.network.ids}"]
  allowed_cidr_blocks     = ["${var.aws_vpc_cidr}"]
  enabled                 = "${var.enabled}"

  # `workers_security_group_count` is needed to prevent `count can't be computed` errors
  workers_security_group_ids   = ["${module.eks_workers.security_group_id}"]
  workers_security_group_count = 1
}

module "eks_workers" {
  source                             = "../../modules/eks-workers"
  namespace                          = "${var.aws_name}"
  stage                              = "${var.aws_env}"
  name                               = "${var.aws_resource}"
  attributes                         = "${var.attributes}"
  tags                               = "${var.tags}"
  eks_worker_ami_name_filter         = "${var.eks_worker_ami_name_filter}"
  instance_type                      = "${var.instance_type}"
  key_name                           = "${var.aws_key_name}"
  vpc_id                             = "${data.aws_vpc.network.id}"
  subnet_ids                         = ["${data.aws_subnet_ids.public.ids}"]
  health_check_type                  = "${var.health_check_type}"
  min_size                           = "${var.min_size}"
  max_size                           = "${var.max_size}"
  wait_for_capacity_timeout          = "${var.wait_for_capacity_timeout}"
  associate_public_ip_address        = "${var.associate_public_ip_address}"
  cluster_name                       = "${module.cluster_label.id}"
  cluster_endpoint                   = "${module.eks_cluster.eks_cluster_endpoint}"
  cluster_certificate_authority_data = "${module.eks_cluster.eks_cluster_certificate_authority_data}"
  cluster_security_group_id          = "${module.eks_cluster.security_group_id}"
  allowed_security_groups            = ["${data.aws_security_groups.network.ids}"]
  allowed_cidr_blocks                = ["${var.aws_vpc_cidr}"]
  enabled                            = "${var.enabled}"

  # Auto-scaling policies and CloudWatch metric alarms
  autoscaling_policies_enabled           = "${var.autoscaling_policies_enabled}"
  cpu_utilization_high_threshold_percent = "${var.cpu_utilization_high_threshold_percent}"
  cpu_utilization_low_threshold_percent  = "${var.cpu_utilization_low_threshold_percent}"
}
