data "aws_vpc" "network" {
  filter {
    name   = "tag:Name"
    values = ["${local.aws_vpc_name}"]
  }
}

data "aws_security_groups" "network" {
  filter {
    name   = "vpc-id"
    values = ["${data.aws_vpc.network.id}"]
  }

  filter {
    name   = "group-name"
    values = ["default"]
  }
}

data "aws_subnet_ids" "public" {
  vpc_id = "${data.aws_vpc.network.id}"

  filter {
    name   = "tag:Name"
    values = ["*public*"]
  }
}

data "aws_subnet_ids" "private" {
  vpc_id = "${data.aws_vpc.network.id}"

  filter {
    name   = "tag:Name"
    values = ["*private*"]
  }
}
