module "aws_vpc" {
  source     = "../../modules/vpc-network"
  namespace  = "${var.aws_name}"
  stage      = "${var.aws_env}"
  name       = "${var.aws_resource}"
  cidr_block = "${var.aws_vpc_cidr}"
}

module "aws_subnets" {
  source              = "../../modules/vpc-subnets"
  namespace           = "${var.aws_name}"
  stage               = "${var.aws_env}"
  name                = "${var.aws_resource}"
  region              = "${var.aws_region}"
  availability_zones  = "${local.aws_vpc_zones}"
  vpc_id              = "${module.aws_vpc.vpc_id}"
  igw_id              = "${module.aws_vpc.igw_id}"
  cidr_block          = "${var.aws_vpc_cidr}"
  nat_gateway_enabled = "${var.aws_nat_enabled}"
}
