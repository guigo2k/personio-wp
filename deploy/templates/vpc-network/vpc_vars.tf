variable "aws_resource" {
  default = "vpc"
}

variable "aws_nat_enabled" {
  default = "true"
}
