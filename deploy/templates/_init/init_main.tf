locals {
  vpc_network_filename = "${dirname(path.module)}/vpc-network/vpc_back.tf"
  eks_cluster_filename = "${dirname(path.module)}/eks-cluster/eks_back.tf"
  efs_storage_filename = "${dirname(path.module)}/efs-storage/efs_back.tf"
  rds_cluster_filename = "${dirname(path.module)}/rds-cluster/rds_back.tf"
}

module "s3_bucket" {
  source            = "../../modules/s3-bucket"
  bucket_name       = "${var.aws_bucket}"
  environment       = "${var.aws_env}"
  description       = "${var.aws_name}-${var.aws_env} Terraform backend"
  bucket_name       = "${var.aws_bucket}"
  bucket_acl        = "${var.bucket_acl}"
  bucket_versioning = "${var.bucket_versioning}"
}

data "template_file" "vpc_network" {
  template = "${file("${path.module}/tmpl/vpc_network.tpl")}"

  vars = {
    aws_name   = "${var.aws_name}"
    aws_env    = "${var.aws_env}"
    aws_bucket = "${var.aws_bucket}"
    aws_region = "${var.aws_region}"
  }
}

data "template_file" "eks_cluster" {
  template = "${file("${path.module}/tmpl/eks_cluster.tpl")}"

  vars = {
    aws_name   = "${var.aws_name}"
    aws_env    = "${var.aws_env}"
    aws_bucket = "${var.aws_bucket}"
    aws_region = "${var.aws_region}"
  }
}

data "template_file" "efs_storage" {
  template = "${file("${path.module}/tmpl/efs_storage.tpl")}"

  vars = {
    aws_name   = "${var.aws_name}"
    aws_env    = "${var.aws_env}"
    aws_bucket = "${var.aws_bucket}"
    aws_region = "${var.aws_region}"
  }
}

data "template_file" "rds_cluster" {
  template = "${file("${path.module}/tmpl/rds_cluster.tpl")}"

  vars = {
    aws_name   = "${var.aws_name}"
    aws_env    = "${var.aws_env}"
    aws_bucket = "${var.aws_bucket}"
    aws_region = "${var.aws_region}"
  }
}

resource "local_file" "vpc_network" {
  content  = "${data.template_file.vpc_network.rendered}"
  filename = "${local.vpc_network_filename}"
}

resource "local_file" "eks_cluster" {
  content  = "${data.template_file.eks_cluster.rendered}"
  filename = "${local.eks_cluster_filename}"
}

resource "local_file" "efs_storage" {
  content  = "${data.template_file.efs_storage.rendered}"
  filename = "${local.efs_storage_filename}"
}

resource "local_file" "rds_cluster" {
  content  = "${data.template_file.rds_cluster.rendered}"
  filename = "${local.rds_cluster_filename}"
}

output "aws_s3_bucket" {
  value = "${module.s3_bucket.bucket_id}"
}
