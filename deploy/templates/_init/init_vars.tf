variable "bucket_name" {
  description = "Name of the S3 bucket"
  default     = ""
}

variable "bucket_acl" {
  description = "Bucket access control"
  default     = "private"
}

variable "bucket_versioning" {
  description = "Bucket object versioning"
  default     = "true"
}

variable "environment" {
  description = "Deployment environment"
  default     = "dev"
}

variable "description" {
  description = "Bucket description"
  default     = "Terraform remote state backend"
}
