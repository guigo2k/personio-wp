terraform {
  backend "s3" {
    bucket = "${aws_bucket}"
    key    = "terraform/${aws_name}-${aws_env}-${aws_region}/rds-cluster.tfstate"
    region = "${aws_region}"
  }
}
