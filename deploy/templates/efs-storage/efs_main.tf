module "efs" {
  source    = "../../modules/efs-filesystem"
  namespace = "${var.aws_name}"
  stage     = "${var.aws_env}"
  name      = "${var.aws_resource}"

  aws_region         = "${var.aws_region}"
  vpc_id             = "${data.aws_vpc.network.id}"
  subnets            = ["${data.aws_subnet_ids.public.ids}"]
  security_groups    = ["${data.aws_security_groups.workers.ids}"]
  availability_zones = ["${local.aws_vpc_zones}"]

  throughput_mode                 = "${var.throughput_mode}"
  provisioned_throughput_in_mibps = "${var.provisioned_throughput_in_mibps}"
}

locals {
  kubeconfig      = "${dirname(path.module)}/eks-cluster/kubeconfig.yaml"
  rbac_file       = "${path.module}/efs-rbac.yaml"
  template_file   = "${file("${path.module}/efs-provisioner.tpl")}"
  deployment_file = "${path.module}/efs-provisioner.yaml"
}

resource "local_file" "persistent_volume" {
  content  = "${data.template_file.persistent_volume.rendered}"
  filename = "${local.deployment_file}"
}

resource "null_resource" "apply_efs_persistent_volume" {
  provisioner "local-exec" {
    command = <<EOF
kubectl apply -f ${local.rbac_file} --kubeconfig ${local.kubeconfig}
kubectl apply -f ${local.deployment_file} --kubeconfig ${local.kubeconfig}
EOF
  }

  depends_on = [
    "module.efs",
    "local_file.persistent_volume",
  ]
}
