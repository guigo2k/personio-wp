output "arn" {
  value       = "${module.efs.arn}"
  description = "EFS ARN"
}

output "id" {
  value       = "${module.efs.id}"
  description = "EFS ID"
}

output "dns_name" {
  value       = "${module.efs.dns_name}"
  description = "EFS DNS name"
}

output "mount_target_dns_names" {
  value       = ["${module.efs.mount_target_dns_names}"]
  description = "List of EFS mount target DNS names"
}

output "mount_target_ids" {
  value       = ["${module.efs.mount_target_ids}"]
  description = "List of EFS mount target IDs (one per Availability Zone)"
}

output "mount_target_ips" {
  value       = ["${module.efs.mount_target_ips}"]
  description = "List of EFS mount target IPs (one per Availability Zone)"
}

output "network_interface_ids" {
  value       = ["${module.efs.network_interface_ids}"]
  description = "List of mount target network interface IDs"
}

output "workers_security_group_id" {
  description = "ID of the worker nodes Security Group"
  value = "${data.aws_security_groups.workers.ids}"
}
