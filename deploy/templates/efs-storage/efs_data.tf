data "aws_vpc" "network" {
  filter {
    name   = "tag:Name"
    values = ["${local.aws_vpc_name}"]
  }
}

data "aws_security_groups" "workers" {
  filter {
    name   = "vpc-id"
    values = ["${data.aws_vpc.network.id}"]
  }

  filter {
    name   = "group-name"
    values = ["*workers*"]
  }
}

data "aws_subnet_ids" "public" {
  vpc_id = "${data.aws_vpc.network.id}"

  filter {
    name   = "tag:Name"
    values = ["*public*"]
  }
}

data "aws_subnet_ids" "private" {
  vpc_id = "${data.aws_vpc.network.id}"

  filter {
    name   = "tag:Name"
    values = ["*private*"]
  }
}

data "template_file" "persistent_volume" {
  template = "${local.template_file}"

  vars {
    FILE_SYSTEM_ID = "${module.efs.id}"
    AWS_REGION     = "${var.aws_region}"
  }
}
