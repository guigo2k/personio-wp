#!/usr/bin/env bash

function error_exit() {
  echo "$1" 1>&2
  exit 1
}

function check_deps() {
  test -f $(command -v kubectl) || error_exit "kubectl not detected in path, please install it"
  test -f $(command -v jq)      || error_exit "jq not detected in path, please install it"
}

function parse_input() {
  # jq reads from stdin so we don't have to set up any inputs, but let's validate the outputs
  eval "$(jq -r '@sh "export service_name=\(.service_name) kubeconfig=\(.kubeconfig)"')"
}

function json_output() {
  local hostname

  # waits nginx-ingress to be assigned an external address (hostname)
  until [[ -n "$hostname" ]]; do
    hostname=$(kubectl get svc "$service_name" --kubeconfig="$kubeconfig" --output=json | jq -r ".status.loadBalancer.ingress[].hostname")
    sleep 3
  done

  # use kubectl and jq to produce a valid json output
  jq -n --arg hostname "$hostname" '{"hostname":$hostname}'
}

check_deps
parse_input
json_output
