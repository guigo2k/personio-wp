# https://www.terraform.io/docs/providers/external/data_source.html
data "external" "nginx_data" {
  program = ["bash", "${path.module}/nginx-data.sh"]

  query = {
    service_name = "lb-nginx-ingress-controller"
    kubeconfig   = "${dirname(path.module)}/eks-cluster/kubeconfig.yaml"
  }
}
