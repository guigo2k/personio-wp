
output "token" {
  value = "${data.aws_eks_cluster_auth.default.token}"
}

# output "nginx_ingress_hostname" {
#   value = "${data.external.nginx_ingress.result.hostname}"
# }
#
# output "nginx_name" {
#   value = "${helm_release.nginx_ingress.name}"
# }
#
# output "nginx_chart" {
#   value = "${helm_release.nginx_ingress.chart}"
# }
