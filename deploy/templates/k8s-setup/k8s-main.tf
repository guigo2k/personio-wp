# https://www.terraform.io/docs/providers/aws/d/eks_cluster_auth.html
provider "kubernetes" {
  host                   = "${data.terraform_remote_state.eks_cluster.eks_cluster_endpoint}"
  cluster_ca_certificate = "${base64decode(data.terraform_remote_state.eks_cluster.eks_cluster_certificate_authority_data)}"
  token                  = "${data.aws_eks_cluster_auth.default.token}"
  load_config_file       = false
}

locals {
  kubeconfig = "${dirname(path.module)}/eks-cluster/kubeconfig.yaml"
}

# Create EKS admin service account
resource "kubernetes_service_account" "eks_admin" {
  count = "${var.enabled == "true" ? 1 : 0}"

  metadata {
    name      = "eks-admin"
    namespace = "kube-system"
  }
}

# Create EKS admin cluster role binding
resource "kubernetes_cluster_role_binding" "eks_admin" {
  count = "${var.enabled == "true" ? 1 : 0}"

  metadata {
    name = "eks-admin"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "eks-admin"
    namespace = "kube-system"
  }
}

# Create Helm/Tiller service account
resource "kubernetes_service_account" "helm_tiller" {
  count = "${var.enabled == "true" ? 1 : 0}"

  metadata {
    name      = "tiller"
    namespace = "kube-system"
  }
}

# Create Helm/Tiller cluster role binding
resource "kubernetes_cluster_role_binding" "helm_tiller" {
  count = "${var.enabled == "true" ? 1 : 0}"

  metadata {
    name = "tiller"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "tiller"
    namespace = "kube-system"
  }
}

# Deploy Tiller (helm init)
resource "null_resource" "helm_tiller" {
  count = "${var.enabled == "true" ? 1 : 0}"

  depends_on = [
    "kubernetes_service_account.helm_tiller",
    "kubernetes_cluster_role_binding.helm_tiller",
  ]

  provisioner "local-exec" {
    interpreter = ["sh", "-c"]

    command = <<EOF
export KUBECONFIG=${local.kubeconfig}
helm init --service-account tiller --history-max 200
EOF
  }
}

resource "helm_release" "nginx_ingress" {
  count      = "${var.enabled == "true" ? 1 : 0}"
  depends_on = ["null_resource.helm_tiller"]

  name       = "lb"
  chart      = "nginx-ingress"
  repository = "${data.helm_repository.stable.metadata.0.name}"
}

# Deploy Cert-Manager
resource "null_resource" "cert_manager" {
  count = "${var.enabled == "true" ? 1 : 0}"

  provisioner "local-exec" {
    interpreter = ["sh", "-c"]

    command = <<EOF
export KUBECONFIG=${local.kubeconfig}
kubectl create namespace cert-manager
kubectl label namespace cert-manager certmanager.k8s.io/disable-validation=true
kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/${var.cert_manager_version}/deploy/manifests/cert-manager.yaml --validate=false
kubectl apply -f letsencrypt-prod.yaml
kubectl apply -f letsencrypt-staging.yaml
EOF
  }
}

# Deploy Kubernetes Dashboard
resource "null_resource" "kube_dashboard" {
  count = "${var.enabled == "true" ? 1 : 0}"

  provisioner "local-exec" {
    interpreter = ["sh", "-c"]

    command = <<EOF
export KUBECONFIG=${local.kubeconfig}
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/${var.kube_dashboard_version}/src/deploy/recommended/kubernetes-dashboard.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/heapster.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/influxdb.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/rbac/heapster-rbac.yaml
EOF
  }
}
