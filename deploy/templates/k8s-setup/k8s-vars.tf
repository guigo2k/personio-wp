variable "enabled" {
  type        = "string"
  description = "Whether to create the resources. Set to `false` to prevent the module from creating any resources"
  default     = "true"
}

variable "cert_manager_version" {
  type        = "string"
  description = "Cert-Manager version (https://github.com/jetstack/cert-manager/releases)"
  default     = "release-0.7"
}

variable "kube_dashboard_version" {
  type        = "string"
  description = "Kubernetes Dashboard version (https://github.com/kubernetes/dashboard/releases)"
  default     = "v1.10.1"
}
