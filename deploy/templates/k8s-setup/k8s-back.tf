terraform {
  backend "s3" {
    bucket = "rz2-infra-dev-us-east-1"
    key    = "terraform/infra-dev-us-east-1/k8s-setup.tfstate"
    region = "us-east-1"
  }
}
