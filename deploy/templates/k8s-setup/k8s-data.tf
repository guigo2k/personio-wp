locals {
  remote_key = "terraform/${var.aws_name}-${var.aws_env}-${var.aws_region}/eks-cluster.tfstate"
  nginx_name = "${helm_release.nginx_ingress.name}-${helm_release.nginx_ingress.chart}-controller"
}

data "terraform_remote_state" "eks_cluster" {
  backend = "s3"

  config = {
    bucket = "${var.aws_bucket}"
    region = "${var.aws_region}"
    key    = "${local.remote_key}"
  }
}

data "aws_eks_cluster" "default" {
  name = "${data.terraform_remote_state.eks_cluster.eks_cluster_id}"
}

data "aws_eks_cluster_auth" "default" {
  name = "${data.terraform_remote_state.eks_cluster.eks_cluster_id}"
}

data "helm_repository" "stable" {
  name = "stable"
  url  = "https://kubernetes-charts.storage.googleapis.com/"
}

data "helm_repository" "incubator" {
  name = "incubator"
  url  = "https://kubernetes-charts-incubator.storage.googleapis.com"
}
