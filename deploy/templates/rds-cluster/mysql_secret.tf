# https://www.terraform.io/docs/providers/aws/d/eks_cluster_auth.html
provider "kubernetes" {
  host                   = "${data.terraform_remote_state.eks_cluster.eks_cluster_endpoint}"
  cluster_ca_certificate = "${base64decode(data.terraform_remote_state.eks_cluster.eks_cluster_certificate_authority_data)}"
  token                  = "${data.aws_eks_cluster_auth.default.token}"
  load_config_file       = false
}

resource "random_string" "auth_key" {
  length  = 64
  special = true
}

resource "random_string" "secure_auth_key" {
  length  = 64
  special = true
}

resource "random_string" "logged_in_key" {
  length  = 64
  special = true
}

resource "random_string" "nonce_key" {
  length  = 64
  special = true
}

resource "random_string" "auth_salt" {
  length  = 64
  special = true
}

resource "random_string" "secure_auth_salt" {
  length  = 64
  special = true
}

resource "random_string" "logged_in_salt" {
  length  = 64
  special = true
}

resource "random_string" "nonce_salt" {
  length  = 64
  special = true
}

resource "kubernetes_secret" "wp_environment" {
  metadata {
    name = "wp-environment"
  }

  data {
    WP_ENV           = "production"
    WP_TITLE         = "${var.wp_title}"
    WP_HOME          = "${var.wp_home}"
    WP_SITEURL       = "${var.wp_siteurl}"
    WP_USER          = "${var.wp_user}"
    WP_EMAIL         = "${var.wp_email}"
    DATABASE_URL     = "${local.database_url}"
    AUTH_KEY         = "${random_string.auth_key.result}"
    SECURE_AUTH_KEY  = "${random_string.secure_auth_key.result}"
    LOGGED_IN_KEY    = "${random_string.logged_in_key.result}"
    NONCE_KEY        = "${random_string.nonce_key.result}"
    AUTH_SALT        = "${random_string.auth_salt.result}"
    SECURE_AUTH_SALT = "${random_string.secure_auth_salt.result}"
    LOGGED_IN_SALT   = "${random_string.logged_in_salt.result}"
    NONCE_SALT       = "${random_string.nonce_salt.result}"
  }
}
