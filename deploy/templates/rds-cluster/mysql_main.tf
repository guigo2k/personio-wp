# https://docs.aws.amazon.com/AmazonRDS/latest/APIReference/API_CreateDBClusterParameterGroup.html
# https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/AuroraMySQL.Updates.20180206.html
# https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/aurora-serverless.html

resource "random_string" "password" {
  length  = 16
  special = false
}

locals {
  db_name      = "${var.aws_name}_${var.aws_env}"
  db_user      = "${var.aws_name}_${var.aws_env}_admin"
  db_password  = "${random_string.password.result}"
  cluster_name = "${module.rds_cluster_aurora_mysql_serverless.cluster_name}"
  endpoint     = "${module.rds_cluster_aurora_mysql_serverless.endpoint}"
  database_url = "mysql://${local.db_user}:${local.db_password}@${local.endpoint}/${local.db_name}"
}

module "rds_cluster_aurora_mysql_serverless" {
  source          = "../../modules/rds-cluster"
  namespace       = "${var.aws_name}"
  stage           = "${var.aws_env}"
  name            = "${var.aws_resource}"
  engine          = "${var.engine}"
  engine_mode     = "${var.engine_mode}"
  cluster_family  = "${var.cluster_family}"
  cluster_size    = "${var.cluster_size}"
  admin_user      = "${local.db_user}"
  admin_password  = "${local.db_password}"
  db_name         = "${local.db_name}"
  db_port         = "${var.db_port}"
  instance_type   = "${var.instance_type}"
  vpc_id          = "${data.aws_vpc.network.id}"
  security_groups = ["${data.aws_security_groups.eks_workers.ids}"]
  subnets         = ["${data.aws_subnet_ids.private.ids}"]

  # zone_id         = "${var.zone_id}"

  scaling_configuration = [
    {
      auto_pause               = true
      max_capacity             = 256
      min_capacity             = 2
      seconds_until_auto_pause = 300
    },
  ]
}
