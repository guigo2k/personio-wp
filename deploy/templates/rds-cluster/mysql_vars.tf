variable "engine" {
  default = "aurora"
}

variable "engine_mode" {
  default = "serverless"
}

variable "cluster_family" {
  default = "aurora5.6"
}

variable "cluster_size" {
  default = 0
}

variable "db_port" {
  default = "3306"
}

variable "instance_type" {
  default = "db.t2.small"
}

variable "aws_resource" {
  default = "mysql"
}
