locals {
  remote_key = "terraform/${var.aws_name}-${var.aws_env}-${var.aws_region}/eks-cluster.tfstate"
}

data "terraform_remote_state" "eks_cluster" {
  backend = "s3"

  config = {
    bucket = "${var.aws_bucket}"
    region = "${var.aws_region}"
    key    = "${local.remote_key}"
  }
}

data "aws_eks_cluster" "default" {
  name = "${data.terraform_remote_state.eks_cluster.eks_cluster_id}"
}

data "aws_eks_cluster_auth" "default" {
  name = "${data.terraform_remote_state.eks_cluster.eks_cluster_id}"
}

data "aws_vpc" "network" {
  filter {
    name   = "tag:Name"
    values = ["${local.aws_vpc_name}"]
  }
}

data "aws_security_groups" "eks_workers" {
  filter {
    name   = "vpc-id"
    values = ["${data.aws_vpc.network.id}"]
  }

  filter {
    name   = "group-name"
    values = ["*eks-workers*"]
  }
}

data "aws_subnet_ids" "private" {
  vpc_id = "${data.aws_vpc.network.id}"

  filter {
    name   = "tag:Name"
    values = ["*private*"]
  }
}

data "aws_subnet_ids" "public" {
  vpc_id = "${data.aws_vpc.network.id}"

  filter {
    name   = "tag:Name"
    values = ["*public*"]
  }
}
