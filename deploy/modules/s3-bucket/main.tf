resource "aws_s3_bucket" "default" {
  bucket = "${var.bucket_name}"
  acl    = "${var.bucket_acl}"

  versioning {
    enabled = "${var.bucket_versioning}"
  }

  tags = {
    Name        = "${var.bucket_name}"
    Environment = "${var.environment}"
    Description = "${var.description}"
  }
}
