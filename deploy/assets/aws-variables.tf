variable "aws_credentials" {
  description = "The path to the shared credentials file (ex. ~/.aws/credentials)"
  default     = "~/.aws/credentials"
}

variable "aws_profile" {
  description = "The AWS profile name as set in the shared credentials file"
  default     = "default"
}

variable "aws_env" {
  type        = "string"
  description = "Environment (e.g. `prod`, `dev`, `staging`)"
}

variable "aws_name" {
  type        = "string"
  description = "Name (e.g. `unilever`, `adidas`)"
}

variable "aws_bucket" {
  type        = "string"
  description = "AWS S3 bucket name (for remote state)"
}

variable "aws_region" {
  type        = "string"
  description = "This is the AWS region"
  default     = "us-east-1"
}

variable "aws_key_name" {
  type        = "string"
  description = "SSH key name that should be used for the instance"
  default     = ""
}

variable "aws_vpc_cidr" {
  type        = "string"
  description = "Classless Inter-Domain Routing block"
}

variable "aws_max_zones" {
  description = "Maximun number of availability zones for this region"
  default     = 3
}

data "aws_availability_zones" "azs" {}

locals {
  aws_vpc_name = "${var.aws_name}-${var.aws_env}-vpc"
  aws_vpc_zones = "${slice(data.aws_availability_zones.azs.names, 0, var.aws_max_zones)}"
}

variable "wp_title" {}
variable "wp_home" {}
variable "wp_siteurl" {}
variable "wp_user" {}
variable "wp_email" {}
