# aws provider
aws_credentials   = "~/.aws/credentials"
aws_profile       = "default"

# aws namespace
aws_name          = "infra"
aws_env           = "dev"
aws_bucket        = "rz2-infra-dev-us-east-1"

# aws region
aws_region        = "us-east-1"
aws_vpc_cidr      = "10.0.0.0/16"
aws_max_zones     = 3

# aws dns (route53)
aws_zone_id       = ""

# aws instances
eks_instance_type = "t2.medium"
rds_instance_type = "db.t2.small"

# wordpress site
wp_title          = "Hello Personio"
wp_home           = "https://personio.aws.tropicloud.net"
wp_siteurl        = "https://personio.aws.tropicloud.net/wp"

# wordpress user
wp_user           = "guigo2k"
wp_email          = "guigo@guigo2k.com"
