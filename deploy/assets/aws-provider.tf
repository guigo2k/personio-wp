
provider "aws" {
  shared_credentials_file = "${var.aws_credentials}"
  profile                 = "${var.aws_profile}"
  region                  = "${var.aws_region}"

  # Make it faster by skipping some checks
  skip_get_ec2_platforms      = true
  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_credentials_validation = true
  skip_requesting_account_id  = true
}
